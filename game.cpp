#include<vector>
#include<string>
#include<iostream>

using namespace std;

class ArithmeticStrategy {
  public:
    virtual int Operate(int) = 0;
    virtual string OperationName() = 0;
};

class NegationStrategy: 
  public ArithmeticStrategy {
  public:
    int Operate(int x) {
      return -x;
    }

    string OperationName() {
      return "-/+";
    }
};

class SubstractionStrategy: 
  public ArithmeticStrategy {
  public:
    int Operate(int x) {
      return x - 5;
    }

    string OperationName() {
      return "-5";
    }
};

class AdditionStrategy:
  public ArithmeticStrategy {
  public:
    int Operate(int x) {
      return x + 8;
    }

    string OperationName() {
      return "+8";
    }
};

class DivisionStrategy: 
  public ArithmeticStrategy {
  public:
    int Operate(int x) {
      int y = 7;
      if(x % 7 == 0)
        return x / 7;
      else
        return x;
    }
    
    string OperationName() {
      return "/7";
    }
};

class Game {
  private:
    int goal;
    vector<ArithmeticStrategy*> actions;
  public:
    Game(int);
    vector<string> FindRoute (vector<string>
      path, int state, int depth);
    void AddStrategy(ArithmeticStrategy*
      strategy);
};

Game::Game (int goal) {
  this->goal = goal;
  this->actions = vector<ArithmeticStrategy*>();
}

void Game::AddStrategy (ArithmeticStrategy*
  strategy) {
  this->actions.push_back(strategy);
}

vector<string> Game::FindRoute (vector<string>
  path, int state, int depth) {
  if(depth == 0) 
    return vector<string>();

  if(state == this->goal)
    return path;

  for(auto strategy = this->actions.begin();
    strategy != this->actions.end(); ++strategy)  {
     vector<string> nextStep = 
       vector<string>(path.size() + 1);
     copy(path.begin(), path.end(),
       nextStep.begin());
     nextStep.push_back(
       (*strategy)->OperationName());
     int newState = (*strategy)->Operate(state);
     if(newState == state)
       continue;

     vector<string> result = 
       this->FindRoute(nextStep, newState,
         depth - 1);

     if(result.size() >0)
       return result;
  }
  return vector<string>();
}


int main(int argc, char** argv) {
  Game* game = new Game(3);
  game->AddStrategy(new DivisionStrategy());
  game->AddStrategy(new NegationStrategy());
  game->AddStrategy(new AdditionStrategy());
  game->AddStrategy(new SubstractionStrategy());
  vector<string> path = 
    game->FindRoute(vector<string>(), 34, 5);
  for( auto step = path.begin(); 
  step != path.end(); ++step)
    cout << *step << '\n';
  return 0;
}
